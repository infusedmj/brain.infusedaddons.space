require('./bootstrap');
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

// Libraries
import Vue from 'vue';
import VueRouter from 'vue-router';
import Buefy from 'buefy'
import 'buefy/lib/buefy.css'


Vue.use(VueRouter);
Vue.use(Buefy);

// Components
import DevelopRelease from './components/DevelopRelease';
import PushManage from './components/PushManage';

let routes = [
    { path: '/develop', component: DevelopRelease },
    { path: '/develop/push', component: PushManage }
];

Vue.use(VueRouter);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

//Vue.component('example-component', require('./components/ExampleComponent.vue'));

const router = new VueRouter({  
    routes  
}); 

const app = new Vue({ 
	el: '#app',
    router,
    mounted: function() {
		setInterval(function () {
	        axios.get('/heartbeat');
	      }, 3000);
    }
});