<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>IA Admin</title>

        <link rel="stylesheet" type="text/css" href="{{ asset('css/admin.css') }}?ver={{filemtime(public_path('css/admin.css')) }}">
       <link rel="shortcut icon" href="https://infusedaddons.com/staging/wp-content/uploads/2016/11/favicon.ico" /> 
        <link rel="apple-touch-icon" href="https://infusedaddons.com/staging/wp-content/uploads/2016/11/apple-icon-180x180.png" />
      
       <link rel="stylesheet" href="//cdn.materialdesignicons.com/2.0.46/css/materialdesignicons.min.css">

    </head>
    <body>
        <div id="app">
        	<nav class="navbar is-dark">
              <div class="navbar-brand">
                <a class="navbar-item" href="/#">
                    <img src="https://infusedaddons.com/staging/wp-content/uploads/2016/10/InfusedAddonsLogo_Official_transpBG.png" />
                </a>
                <div class="navbar-burger burger" data-target="navbarExampleTransparentExample">
                  <span></span>
                  <span></span>
                  <span></span>
                </div>
              </div>

              <div id="navbarExampleTransparentExample" class="navbar-menu ">
                <div class="navbar-start">
                  <router-link to="/" class="navbar-item">
                    Dashboard
                  </router-link>
                  <div class="navbar-item has-dropdown is-hoverable">
                    <router-link to="/develop" class="navbar-link">
                      Develop
                    </router-link>
                    <div class="navbar-dropdown">
                      <router-link to="/develop" class="navbar-item">
                         Release Plugin
                      </router-link>
                      <router-link to="/develop/product" class="navbar-item">
                         Product
                      </router-link>
                      <router-link to="/develop/push" class="navbar-item">
                         Push Notify
                      </router-link>
                    </div>
                  </div>
                </div>


                <div class="navbar-end">
                  <div class="navbar-item has-dropdown is-hoverable">
                    <a class="navbar-link">
                      <i class="mdi mdi-account mdi-24px"></i>
                    </a>

                    <div class="navbar-dropdown is-right">
                      <!--<hr class="navbar-divider">-->
                      <div class="navbar-item">
                        Log Out
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </nav>
            <router-view></router-view>
        </div>

        <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    	 <script type="text/javascript" src="{{ asset('js/admin.js') }}?ver={{filemtime(public_path('js/admin.js')) }}"></script>

         <script type="text/javascript">
             document.addEventListener('DOMContentLoaded', function () {

              // Get all "navbar-burger" elements
              var $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

              // Check if there are any navbar burgers
              if ($navbarBurgers.length > 0) {

                // Add a click event on each of them
                $navbarBurgers.forEach(function ($el) {
                  $el.addEventListener('click', function () {

                    // Get the target from the "data-target" attribute
                    var target = $el.dataset.target;
                    var $target = document.getElementById(target);

                    // Toggle the class on both the "navbar-burger" and the "navbar-menu"
                    $el.classList.toggle('is-active');
                    $target.classList.toggle('is-active');

                  });
                });
              }

            });
         </script>
        
    </body>

</html>