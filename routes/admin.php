<?php

use Illuminate\Http\Request;

Auth::routes();

Route::get('/', 'AdminController@getIndex')->middleware('auth');
Route::get('/heartbeat', 'AdminController@heartbeat')->middleware('auth');

Route::apiResource('products', 'API\ProductController')->middleware('auth');
Route::apiResource('releases', 'API\ReleaseController')->middleware('auth');

Route::get('subscriptions', 'API\ReleaseController@subscriptions_get')->middleware('auth');
Route::get('search_subscriber', 'API\ReleaseController@subscriber_search')->middleware('auth');
Route::get('push_msgs', 'API\ReleaseController@pushmsgs_get')->middleware('auth');
Route::get('task_queue', 'API\ReleaseController@task_queue')->middleware('auth');

Route::post('push_notice', 'API\ReleaseController@post_push_notice')->middleware('auth');


Route::get('releases/download/{product}/{release}','API\ReleaseController@request_download')->middleware('auth');
Route::post('releases/change_status/{status}/{release}','API\ReleaseController@change_status')->middleware('auth');

