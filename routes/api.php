<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('push_subscribe', 'PushController@subscribe');
Route::get('push_feed/{sku}', 'PushController@feed');

Route::post('sns_confirm', 'PushController@sns_confirm');
Route::post('register_site', 'LicenseController@request_activate_installation');
Route::post('update_file_request','LicenseController@request_download_update');
Route::post('site_deactivation', 'LicenseController@request_deactivate_installation');





/*
Route::post('update_file_request', function() {
	return json_encode(array(
		'error' 	=> 'License key has already expired.',
		
	));

	return json_encode(array(
		'download_url' 	=> 'http://infusedaddons.space/infusedwoo.zip',
		'expires'		=> date('Y-m-d H:i:s', time() + 30*3600)
	));
});

*/

