<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQueuedtasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('queued_tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('task',200);
            $table->longText('data')->nullable();
            $table->integer('lck')->nullable();
            $table->longText('log')->nullable();
            $table->integer('priority')->nullable();
            $table->string('status',100);
            $table->string('src',100)->nullable();
            $table->string('recurring',100)->nullable();
            $table->dateTime('lock_expire')->nullable();
            $table->dateTime('scheduled_exec')->nullable();
            $table->dateTime('last_performed')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('queued_tasks');
    }
}
