<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePushTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('push', function (Blueprint $table) {
            $table->increments('id');
            $table->string('push_title');
            $table->string('type');
            $table->longText('msg');
            $table->string('meta1',500);
            $table->string('meta2',500);
            $table->longText('meta3');
            $table->string('subscription');
            $table->dateTime('release_date');
            $table->dateTime('push_date');
            $table->boolean('has_errors');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('push');
    }
}
