<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDebuglogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('debug_log', function (Blueprint $table) {
            $table->increments('id');
            $table->string('src',100)->nullable();
            $table->string('type',100);
            $table->longText('msg');
            $table->integer('severity')->nullable();
            $table->bigInteger('microtime');
            $table->longText('backtrace')->nullable();
            $table->text('client')->nullable();
            $table->boolean('temp')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('debug_log');
    }
}
