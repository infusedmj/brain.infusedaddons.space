<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSnsStatusCol extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pushsubs', function (Blueprint $table) {
            $table->string('sns_status',10);
            $table->string('sns_id',100);
            $table->string('sns_unsubscribe',400);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pushsubs', function (Blueprint $table) {
            $table->dropColumn('sns_status');
            $table->dropColumn('sns_id');
            $table->dropColumn('sns_unsubscribe');
        });
    }
}
