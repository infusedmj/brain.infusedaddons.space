<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePusherrorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pusherrors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pushsub_id')->unsigned();
            $table->integer('push_id')->unsigned();
            $table->longText('errorinfo');
            $table->integer('errortimes');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pusherrors');
    }
}
