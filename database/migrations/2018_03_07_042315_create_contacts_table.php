<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fname', 400);
            $table->string('lname',400);
            $table->string('email',191)->unique();
            $table->integer('account_id')->unsigned();
            $table->string('password',191);
            $table->string('contact_type',40);
            $table->string('phone',100);
            $table->string('timezone',10);
            $table->string('language',100);
            $table->string('address1',400);
            $table->string('address2',400);
            $table->string('address3',400);
            $table->string('city',400);
            $table->string('state',400);
            $table->string('country',400);
            $table->string('country_code',3);
            $table->string('mobile',100);
            $table->string('fax',100);
            $table->string('zip',10);
            $table->string('website',300);
            $table->string('mobiletoken',400);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
