<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QueuedTask extends Model
{
    protected $fillable = array('task', 'data', 'lck', 'log','priority','status','src','scheduled_exec','last_performed','lock_expire', 'recurring');

    function lock($expires_in_sec = 20) {
        $this->lck = 1;
        $this->lock_expire = date('Y-m-d H:i:s', time() + $expires_in_sec);
        $this->save();
    }

    function unlock() {
        $this->lck = 0;
        $this->save();
    }

    function report_error($log="", $status='failed') {
        $this->status = $status;
        $this->log = (isset($this->log) && !empty($this->log)) ? ($log . "\n\n\n" . $this->log) : $log;
        $this->last_performed = date('Y-m-d H:i:s');

        $this->save();
    }

    function update_data($key,$val) {
        $data = maybe_unserialize( $this->data );
        if(!is_array($data)) $data = array();
        $data[$key] = $val;
        $this->data = serialize($data);

    }

    function schedule($time='now', $recur=false) {
        if($time == 'now') $time = time();

        if(!empty($time)) {
            $this->scheduled_exec = date('Y-m-d H:i:s', $time);
            $this->status = 'scheduled';
        } else {
            $this->scheduled_exec = date('Y-m-d H:i:s', time()-10000);
        }
        
        if($this->recurring !== false) $this->recurring = $recur;
        $this->save();
    }

    function get_data($key, $default=false) {
        $data = maybe_unserialize( $this->data );
        return isset($data[$key]) ? $data[$key] : $default;
    }

    function done() {
        if($this->recurring) {
            $exec_timestamp = strtotime($this->scheduled_exec);
            if($exec_timestamp == 0) $exec_timestamp = time();

            $this->status = 'scheduled';

            switch ($this->recurring) {
                case 'hourly':
                    $next_scheduled_exec = date('Y-m-d H:i:s', $exec_timestamp + 3600);
                    break;
                case 'daily':
                    $next_scheduled_exec = date('Y-m-d H:i:s', $exec_timestamp + 3600*24);
                    break;
                case 'weekly':
                    $next_scheduled_exec = date('Y-m-d H:i:s', $exec_timestamp + 7*3600*24);
                    break;
                case 'biweekly':
                    $next_scheduled_exec = date('Y-m-d H:i:s', $exec_timestamp + 14*3600*24);
                    break;
                case 'monthly':
                    $next_scheduled_exec = date("Y-m-d H:i:s", strtotime("+1 month", $exec_timestamp));
                    break;
                case 'yearly':
                    $next_scheduled_exec = date("Y-m-d H:i:s", strtotime("+1 year", $exec_timestamp));
                    break;
                default:
                    if(!empty($this->recurring)) {
                        $next_scheduled_exec = date("Y-m-d H:i:s", strtotime("+" . $this->recurring, $exec_timestamp));
                    } else {
                        $this->status = 'done';
                    }
                    
                    break;
            }

            if(isset($next_scheduled_exec)) {
                if(strtotime($next_scheduled_exec) < time()) {
                    $next_scheduled_exec = date('Y-m-d H:i:s');
                }

                $this->scheduled_exec = $next_scheduled_exec;
            }
        } else {
            $this->status = 'done';     
        }

        
        $this->last_performed = date('Y-m-d H:i:s');
        $this->save();
    }
}