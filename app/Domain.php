<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Domain extends Model
{
    protected $fillable = [
        'domain',
		'fullpath',
		'license_id',
		'activationkey',
		'status',
		'ip',
		'activation_date'
    ];

    public function activities()
    {
        return $this->hasMany('App\Activity');
    }

    public function license() {
        return $this->belongsTo('App\License')->withDefault();
    }
}
