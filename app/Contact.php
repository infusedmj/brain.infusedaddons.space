<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fname',
		'lname',
		'email',
		'account_id',
		'password',
		'contact_type',
		'phone',
		'timezone',
		'language',
		'address1',
		'address2',
		'address3',
		'city',
		'state',
		'country',
		'country_code',
		'mobile',
		'fax',
		'zip',
		'website',
		'mobiletoken'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','mobiletoken'
    ];

    public function activities()
    {
        return $this->hasMany('App\Activity');
    }

    public function account() {
        return $this->belongsTo('App\Account');
    }
}
