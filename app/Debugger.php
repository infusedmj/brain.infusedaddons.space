<?php

namespace App;

use Illuminate\Http\Request;

class Debugger {
	private $temp_logs = array();
	private $live_check_transient = '_debug_live';
	private $monitor_errors = false;
	private $scopes = array();
	private $parent = false;

	# Debug Level 0 = No logging at all
	# Debug Level 1 = Only Fatal Errors with backtrace
	# Debug Level 2 = Include Warnings and Notices
	# Debug Level 3 = Include Function Logs
	# Debug Level 4 = Include backtrace always

	function __construct($source='', $monitor_errors = true, $user_identifier='', $debug_level=false, $parent=false) {
		$this->start_us = microtime(true);
		$this->source = $source;

		if($debug_level !== false) $this->debug_level = $debug_level;
		else $this->debug_level = Option::get('debug_level', 1);

		if(empty($user_identifier)) {
			$this->user = "";
		}

		// Clear Cache
		if($monitor_errors) {
			$this->monitor_errors = true;
			set_error_handler(array($this,	'handle_errors'));
		}

		if($parent !== false) {
			$this->parent = $parent;
		}
 	}

	function is_live() {
		if(!isset($this->live)) {
			$this->live = Option::get( $this->live_check_transient );
		}

		return $this->live;
		
	}

	public function log($type, $message='', $backtrace=false, $severity='',$temp_only=false) {
		$log = array(
				'type' => $type,
				'msg' => $message,
				'backtrace' => $backtrace,
				'severity' => $severity
			);

		if(!$temp_only) {
			if($this->should_save_log($log)) {
				$this->save_log($log);
			} else if($this->is_live()) {
				$this->save_log($log, true);
			}
		}

		$this->temp_logs[] = $log;

		if($this->parent !== false) {
			$log['msg'] = "{$this->source} > " . $log['msg'];
			$this->parent->log($log['type'],$log['msg'],false,$log['severity'],true);
		}
	}

	public function get_log_summary() {
		$error_summary = array();
		$error_summary[] = 'SRC: ' . $this->source;
		$error_summary[] = 'User: ' . $this->user;
		$error_summary[] = 'Time: ' . date('Y-m-d H:i:s');

		foreach($this->temp_logs as $log) {
			$error_summary[] = "\t" . $log['msg'];
		}

		return implode("\n", $error_summary);
	}


	public function save_log($log, $temp=false) {
		$debug_log = new DebugLog;
		$debug_log->src = $this->source;
		$debug_log->type = $log['type'];
		$debug_log->msg = $log['msg'];
		$debug_log->severity = $log['severity'];
		$debug_log->client = $this->user;
		$debug_log->microtime = microtime(true) * 1000000;

		$record_trace = $log['backtrace'] || ($this->debug_level >= 4);

		if($record_trace) {
			$debug_log->backtrace = $this->generateCallTrace(2);
		}

		if($temp) {
			$debug_log->temp = 1;
		} else {
			$debug_log->temp = 0;
		}

		$debug_log->save();
	}

	public function should_save_log($log) {
		switch ($this->debug_level) {
			case 1:
				if($log['severity'] >= 3) return true;
				break;
			
			case 2:
				if($log['severity'] >= 1) return true;
				break;

			case 3:
			case 4:
				if($log['severity'] >= 0) return true;
				break;

			default:
				return false;
				break;
		}

		return false;
	}

	public function handle_errors($errno, $errstr, $errfile, $errline) {
		$errmsg = '';
		$backtrace = false;

	    switch ($errno) {
		    case E_USER_ERROR:
		        $errmsg .= "** FATAL ERROR ** $errstr $errfile($errline), PHP " . PHP_VERSION . " (" . PHP_OS . ")";
		        $type = 'fatal_error';
		        $severity = 3;
		        $backtrace = true;
		        break;

		    case E_USER_WARNING:
		   		$errmsg .= "* WARNING * $errstr $errfile($errline)";
		   		$type = 'warning';
		   		$severity = 2;
		        break;

		    case E_USER_NOTICE:
		        $errmsg .= "* NOTICE * $errstr $errfile($errline)";
		        $type = 'notice';
		        $severity = 1;
		        break;

		    default:
		        $errmsg .= "* UNEXPECTED ERROR * $errstr $errfile($errline)";
		        $type = 'warning';
		        $severity = 2;
		        $backtrace = true;
		        break;
		}

		if(empty($errno)) {
			$errmsg .= "* NOTICE * $errstr $errfile($errline)";
	        $type = 'notice';
	        $severity = 1;
		}

		$this->log($type, $errmsg, $backtrace, $severity);
	    return true;
	}

	public function generateCallTrace($skip_number_of_calls = 0) {
	    $e = new \Exception();
	    $trace = explode("\n", $e->getTraceAsString());
	    // reverse array to make steps line up chronologically
	    $trace = array_reverse($trace);
	    array_shift($trace); // remove {main}
	    array_pop($trace); // remove call to this method
	    for($i = 0; $i < $skip_number_of_calls; $i++) {
	    	array_pop($trace);
	    }

	    $length = count($trace);
	    $result = array();
	    
	    for ($i = 0; $i < $length; $i++) {
	        $result[] = ($i + 1)  . ')' . substr($trace[$i], strpos($trace[$i], ' '));
	    }

	    $result[] = "FULL URL: " . Request::url();
	    
	    return "\t" . implode("\n\t", $result);
	}

	public function reported_error($severity = 3) {
		foreach($this->temp_logs as $log) {
			if($log['severity'] >= $severity) {
				return true;
			}
		}

		return false;
	}

	public function get_logs() {
		return $this->temp_logs;
	}

	public function end() {
		if($this->monitor_errors) {
			restore_error_handler();
		}

		$end_us = microtime(true);
		$spent_ms = number_format(($end_us - $this->start_us)*1000.0, 2);

		$this->log(	'debug_summary',
					"Summary: Process total spent time: {$spent_ms} ms" 
				);
	}

	public function new_scope($prefix = "") {
		if(!empty($prefix)) {
			$new_scope_src = $prefix;
		}

		$child_debugger = new Debugger($new_scope_src, $this->monitor_errors, $this->user, $this->debug_level, $this);
		$this->scopes[] = $child_debugger;

		return $child_debugger;
	}
}

