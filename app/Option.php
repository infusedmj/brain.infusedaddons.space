<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Option extends Model
{
	protected $fillable = [
		'option_key',
		'option_val',
		'autoload',
		'expires'
	];

	public static function get($key, $default=false) {
		$val = self::where('option_key',$key)->first();

		if($val) {
			$now = \Carbon\Carbon::now();

			if(!empty($val->expires)) {
				$expire_date = new \Carbon\Carbon($val->expires);
				if($expire_date->gt($now)) {
					return $default;
				}
			}

			$unser = Util::maybe_unserialize($val->option_value);

			if($unser === false) return $val;
			else return $unser;
		} else {
			return $default;
		}
		
	}

	public static function set($key, $val, $expires=0) {
		$option = self::where('option_key',$key)->first();

		if(!$option) {
			$option = new Option;
			$option->option_key = $key;
		}
		
		$val = Util::maybe_serialize($val);
		$option->option_val = $val;

		if($expires > 0) {
			$expire_date = \Carbon\Carbon::now()->addSeconds($expires);
			$option->expires = $expire_date;
		} else {
			$option->expires = NULL;
		}

		return $option->save();
	}
}

