<?php

namespace App;

class Task {
	protected $tq;
	protected $data = '';
	protected $debugger;
	protected $is_scheduled;

	public function do_task() { }


	public function __construct($data = false, $task_queue = false, $debugger= false) {
		if($data !== false) {
			$this->data = $data;
		} else {
			$this->data = array();
		}

		if($task_queue !== false) {
			$this->tq = $task_queue;
		}

		if($debugger !== false ) {
			$this->parent_debugger = $debugger;
		}
	} 

	

	public function data($data = false) {
		if($data !== false) {
			$this->data = $data;
		}
	}
	
	public function is_a_process() {
		return false;
	}

	public function auto_do() {
		if($this->is_scheduled) {
			throw new Exception("Scheduled Tasks cannot be manually run.");
			return false;
		}

		$src = $this->get_src();
		
		if(!isset($this->parent_debugger)) {
			$debugger = new Debugger($src);
		} else {
			$debugger = $this->parent_debugger->new_scope($src);
		}

		$this->debugger = $debugger;
			$this->do_task();
		$debugger->end();
		

		if($debugger->reported_error()) {
			$error_summary = $debugger->get_log_summary();

			$times_error = isset($this->data['error_times']) ? $this->data['error_times'] + 1 : 1;
			$this->data['error_times'] = $times_error; 

			// Reschedule
			$if_resked = Option::get('reschedule_failed_tasks', true);
			$schedule = Option::get('failed_tasks_schedule_mins', array(30,60,120,240,990));

			if($if_resked) {
				if(isset($schedule[$times_error-1])) {
					$next = time() + 60*$schedule[$times_error-1];
 					$this->schedule(date('Y-m-d H:i:s', $next));
 					$status = 'failed';
				} else {
					$status = 'error';
				}					
			}

		
			if(isset($this->tq)) {
				$this->tq->report_error($error_summary, $status);
				$this->tq->unlock();
			}
		} else {
			if(isset($this->tq)) {
				$this->tq->done();
				$this->tq->unlock();
			}
		}
	}

	public function get_src() {
		if(isset($this->data['src'])) {
			return $this->data['src'];
		} else {
			return get_class($this);
		}
	}

	public function schedule($time='', $recur = false) {
		if(empty($time)) $time = '';
		else if($time == 'now') $time = time();
		else $time = strtotime($time);

		if(isset($this->tq)) {
			$this->tq->data = serialize( $this->data );
			$this->tq->schedule($time, $recur);
		} else {
			$tq = new QueuedTask;
			$tq->task = get_class($this);
			$tq->data = serialize( $this->data );
			$tq->status = 'open';

			$tq->lck = 0;
			$tq->priority = 0;
			$tq->log = '';
			$tq->src = $this->get_src();
			
			if(empty($time)) $tq->scheduled_exec = date('Y-m-d H:i:s', time()-10000);
			else $tq->scheduled_exec = date('Y-m-d H:i:s', $time);
			
			if($recur) $tq->recurring = $recur;
			else $tq->recurring = '';
			$s = $tq->save();

			$this->tq = $tq;
		}

		$this->is_scheduled = true;
	}

	public function has_been_scheduled() {
		$task = get_class($this);
		return QueuedTask::where('task', '=', $task)->where('status', '!=', 'done')->count();
	}

	public function clear_scheduled_tasks() {
		$task = get_class($this);
		QueuedTask::where('task', '=', $task)->where('status', '!=', 'done')->delete();
	}

	public function schedule_once($time='', $recur = false) {	
		$exists = $this->has_been_scheduled();

		if(!$exists) {
			$this->schedule($time, $recur);
		}
	}

	public function get_data($key, $fallback=false) {
		if(isset($this->data[$key])) {
			return $this->data[$key];
		} else {
			return $fallback;
		}
	}

	public function lock() {
		if(isset($this->tq)) {
			$this->tq->lock();
		}
	}

	public function unlock() {
		if(isset($this->tq)) {
			$this->tq->last_performed = date('Y-m-d H:i:s');
			$this->tq->unlock();
		}
	}

	public function set_data_val($key,$value, $save=true) {
		if(!is_array($this->data)) {
			$this->data = array($key => $value);
		} else {
			$this->data[$key] = $value;
		}

		if($save) {
			$this->save_data();
		}
	}

	public function save_data() {
		if(isset($this->tq)) {
			$this->tq->data = serialize( $this->data );
			$this->tq->save();
		}
	}
}
