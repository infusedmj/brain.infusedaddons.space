<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class License extends Model
{
    protected $fillable = [
        'account_id',
		'product_id',
		'licensekey',
		'type',
		'status',
		'expires',
		'domain_limit'
    ];

    public function activities()
    {
        return $this->hasMany('App\Activity');
    }

    public function domains()
    {
        return $this->hasMany('App\Domain');
    }

    public function domains_grouped()
    {
        $domains = $this->domains()->where('status','active')->get();

        $group = array();
        foreach($domains as $domain) {
            $group[$domain->domain] = $domain;
        }

        return $group;
    }

    public function account() {
        return $this->belongsTo('App\Account')->withDefault();
    }

    public function product() {
        return $this->belongsTo('App\Product')->withDefault();
    }

    public function pushsubs()
    {
        return $this->hasMany('App\Pushsub');
    }
}
