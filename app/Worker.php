<?php

namespace App;

class Worker {

	private $tasks;
	private $pending_process;
	private $pending_process_error_count;

	function __construct($expiration, $tasks = array()) {
		$this->tasks = $tasks;
		$this->start_time = time();
		$this->expiration = $expiration;
		$this->do_tasks();
	}

	function secure_next_task() {
		if(count($this->tasks) > 0) {
			$next_task = $this->tasks[0];
			array_shift($this->tasks);
			return $next_task;
		} else {
			if(isset($this->pending_process) && !$this->pending_process->is_complete() && !$this->pending_process->failed) {
				$item = $this->pending_process;
			} else {
				$this->pending_process = NULL;
				
				$item = WorkerManager::get_next_available_task();

				if(!$item) {
					return false;
				} else if($item->is_a_process()) {
					$this->pending_process = $item;
				}
			}

			return $item;
		}

		return false;
	}

	function do_tasks() {
		$start_time = time();

		do {
			// note each task should only last max 2 sec and max 9% of memory
			$task = $this->secure_next_task();

			if($task !== false) {
				$task->auto_do();
			} else {
				break;
			}

		} while(!$this->time_exceeded() && !$this->memory_exceeded());

		if(isset($this->pending_process)) {
			$this->pending_process->unlock();
		}
	}

 	function time_exceeded() {
		$finish = $this->start_time + $this->expiration; 
		$return = false;
		if ( time() >= $finish ) {
			$return = true;
		}

		return $return;
	}


	function memory_exceeded() {
		$memory_limit   = WorkerManager::get_memory_limit() * 0.9; // 90% of max memory
		$current_memory = memory_get_usage( true );
		$return         = false;
		if ( $current_memory >= $memory_limit ) {
			$return = true;
		}
		return $return;
	}
}