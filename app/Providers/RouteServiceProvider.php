<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapAdminRoutes();

        $this->mapWebRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        if(env('APP_ENV') == 'local') {
            Route::domain('api.infusedaddons.space')
                 ->middleware('api')
                 ->namespace($this->namespace)
                 ->group(base_path('routes/api.php'));
        } else {
            Route::domain('api.infusedaddons.com')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));  
        }

        

        Route::domain('infusedaddons-space.us-east-1.elasticbeanstalk.com')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));

    }

    protected function mapAdminRoutes()
    {
        if(!defined('RDS_DB_NAME') == 'local') {
            Route::domain('admin.infusedaddons.space')
             ->middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/admin.php'));
         } else {
             Route::domain('admin.infusedaddons.com')
             ->middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/admin.php'));
         }
        

       
    }
}
