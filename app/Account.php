<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
     protected $fillable = [
        'company_name',
		'account_name',
		'account_code',
		'website',
		'country',
		'status'
    ];

    public function activities()
    {
        return $this->hasMany('App\Activity');
    }

    public function contacts()
    {
        return $this->hasMany('App\Contact');
    }

    public function licenses()
    {
        return $this->hasMany('App\License');
    }
}
