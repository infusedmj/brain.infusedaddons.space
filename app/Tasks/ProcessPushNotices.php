<?php

namespace App\Tasks;

use App\Push;
use App\Pushsub;
use \Carbon\Carbon;

class ProcessPushNotices extends \App\Process {
	public function do_task($item = array(), $debugger=false) { 
		$status = $this->get_data('status','sns');
		$push_id = $this->get_data('id');
		$push = Push::find($push_id);
		
		if($push->subscription()->first()) {
			$total_to_push = $push->subscription()->first()->pushsubs()->count();
		} else {
			$total_to_push = 1;
		}

		if(!$total_to_push) $total_to_push = 1;
		$total_pushed = $this->get_data('total_pushed',0);
		$pushed_to = $this->get_data('pushed_to',array());


		$push_controller = new \App\Http\Controllers\PushController;

		if($item == 'sns') {
			$push_controller->sns_push($push);
			$this->set_data_val('status','normalpush');
			$pushed = $push->subscription()->first()->pushsubs()->where('sns_status', 'active')->count();
			$pushed_to[] = 'sns';
		} else if($item == 'done') {
			$push->push_date = Carbon::now();
			$push->status = $this->get_data('final_status','Done');
			$push->save();
			$this->set_data_val('status','done');
		} else {
			$push_controller->push($push,$item);
			$pushed = 1;
			$pushed_to[] = $item->id;
			$this->set_data_val('last_push_id',$item->id);
		}

		$total_pushed += $pushed;
		$percent = round($total_pushed * 100 / $total_to_push);

		$this->set_data_val('total_pushed', $total_pushed);
		$this->set_data_val('pushed_to', $pushed_to);
		$this->set_data_val('percent', $percent);
	}
	
	public function get_next_item() { 
		$status = $this->get_data('status','sns');
		$push_id = $this->get_data('id');
		$push = Push::find($push_id);

		if($status != 'sns' && $status != 'custom') {
			$last_push_id = $this->get_data('last_push_id',0);
			$next_to_push = $push->subscription()->first()->pushsubs()->where('sns_status', '!=', 'active')->where('status','active')->where('id','>',$last_push_id)->orderBy('id','asc')->first();

			if($next_to_push) {
				return $next_to_push;
			} else {
				return 'done';
			}	
		} else if($status == 'custom') {
				return 'done';
		} else {
			if($push->meta2) {
				$next_to_push = Pushsub::find($push->meta2);
				$this->set_data_val('status','custom');
				return $next_to_push;
			} else {
				return 'sns';
			}
			
		}
	}

	public function is_complete() { 
		$status = $this->get_data('status');
		return $status == 'done';
	}
}