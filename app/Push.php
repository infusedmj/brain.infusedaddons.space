<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Push extends Model
{
	protected $table = 'push';
    protected $fillable = [
    	'push_title',
		'type',
		'msg',
		'meta1',
		'meta2',
		'meta3',
		'subscription',
		'release_date',
		'push_date',
		'has_errors',
		'status'
    ];

    public function pusherrors()
    {
        return $this->hasMany('App\Pusherror');
    }

    public function subscription() {
        return $this->belongsTo('App\Subscription','subscription');
    }

    public function report_error($pushsub,$error,$status='pending') {
    	if(!$this->id) {
            $this->save();
    		return false;
    	}

    	$pe = Pusherror::where('pushsub_id',$pushsub_id)->where('push_id',$this->id)->first();
    	$errortimes = 1;

    	if($pe) {
    		$pe->errorinfo = $error;
    		$pe->errortimes++;
    		$pe->status = $status;
    		$errortimes = $pe->errortimes;
    		$pe->save();
    	} else {
			$pe = new Pusherror;
    		$pe->pushsub_id = $pushsub->id;
    		$pe->push_id = $this->id;
    		$pe->status = $status;
    		$pe->errortimes = 1;
    		$pe->save();
    	}

    	$this->has_errors = 1;
    	$this->save();

    	return $errortimes;
    }

    public function prep_push_msg() {
        $msg = array(
            'push_title'    => $this->push_title,
            'type'          => $this->type,
            'msg'           => $this->msg,
            'origin'        => 'InfusedAddons',
            'release'       => $this->release_date,
            'meta'          => $this->meta3
        );

        if($this->id) $msg['push_key'] = 'InfusedWooUpdates' . $this->id;
        return $msg;
    }
}
