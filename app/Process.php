<?php

namespace App;

class Process extends Task {
	public $failed = false;

	public function do_task($item = array(), $debugger=false) { }
	public function get_next_item() { }
	public function is_complete() { }


	public function is_a_process() {
		return true;
	}
	public function auto_do() {
		if($this->is_scheduled) {
			throw new Exception("Scheduled Processes cannot be manually run.");
			return false;
		}

		if($this->is_complete()) {
			if(isset($this->tq)) {
				$this->tq->done();
				$this->tq->unlock();
				return true;
			}
		};

		$src = $this->get_src();		
		$this->debugger = new Debugger($src);
		$debugger = $this->debugger;

			$item = $this->get_next_item();
			$this->do_task($item, $debugger);

		$debugger->end();

		if($this->is_complete()){
			if(isset($this->tq)) {
				$this->tq->done();
				$this->tq->unlock();
				return true;
			}
		} else if($debugger->reported_error()) {
			$times_error = isset($this->data['error_times']) ? $this->data['error_times'] + 1 : 1;
			$consecutive_errors = isset($this->data['consecutive_errors']) ? $this->data['consecutive_errors'] + 1 : 1;
			$this->data['error_times'] = $times_error; 
			$this->data['consecutive_errors'] = $consecutive_errors;

			// Reschedule
			$if_resked = Option::get('reschedule_failed_processes', true);
			$resked_on_consecutive_error = Option::get('no_ce_to_resked_process', 5);
			$schedule = Option::get('mins_to_resked_process', 360);
			$error_summary = $debugger->get_log_summary();

			if($this->is_complete()) {
				$this->tq->done();
				$this->tq->unlock();
			} else if($if_resked && $consecutive_errors >= $resked_on_consecutive_error ) {
				$this->failed = true;

				if($schedule > 0) {
					$this->data['consecutive_errors'] = 0;
					$next = time() + 60*$schedule;
 					$this->schedule(date('Y-m-d H:i:s', $next));
 					$status = 'failed';
				} else {
					$status = 'error';
				}	

				if(isset($this->tq)) {
					$this->tq->report_error($error_summary, $status);
					$this->tq->unlock();
				}				
			} else {
				$this->tq->report_error($error_summary, 'open');
			}

			$this->save_data();
		} else {
			$this->data['consecutive_errors'] = 0;

			if(isset($this->tq)) {
				$this->save_data();

				if($this->is_complete()) {
					$this->tq->done();
					$this->tq->unlock();
				}
			}
		}
	}



}
