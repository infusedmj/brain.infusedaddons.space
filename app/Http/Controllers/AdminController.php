<?php

namespace App\Http\Controllers;

class AdminController extends Controller
{ 
	function getIndex() {
		return view('admin');
	}

	function heartbeat() {
		\App\WorkerManager::maybe_deploy_worker();
	}
}