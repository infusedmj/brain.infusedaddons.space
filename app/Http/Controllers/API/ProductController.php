<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        return json_decode( $products );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product = new Product;
        $product_name = $request->input('name');

        if(strpos($product_name, '|') !== false) {
            $pr = explode('|', $product_name);
            $product->name = $pr[0];
            $product->sku = $pr[1];
        } else {
            $product->name = $product_name;
            $product->sku = $request->input('sku');
        }

        $product->status = 'active';
        $product->downloadurl1 = '';
        $product->downloadurl2 = '';
        $product->downloadurl3 = '';
        $product->guideurl1 = '';
        $product->guideurl2 = '';

        $product->save();

        // ADD SUBSCRIPTIONS FOR TEST, STABLE AND META
        $test_sub = new \App\Subscription;
        $test_sub->create_new($product->id, $product->sku . '_test', 'test', $product_name . ' Test');

        $beta_sub = new \App\Subscription;
        $beta_sub->create_new($product->id, $product->sku . '_beta', 'beta', $product_name . ' Beta');


        $stable_sub = new \App\Subscription;
        $stable_sub->create_new($product->id, $product->sku, 'stable', $product_name . ' Stable');

        return json_encode(['success' => 1]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
