<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Activity;
use App\Push;
use \Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class ReleaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $push_msgs = Push::where('type','pluginupdate')->orderBy('push_date','desc')->get();
        $releases = array();

        foreach($push_msgs as $p) {
            $r = $this->push_to_release($p);
            $releases[] = $r;
        }

        return json_encode( $releases );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product = $request->input('product');
        $version = $request->input('version');
        $folder = "plugin_downloads/$product/$version";
        $filename = "{$product}.zip";

        if ($request->hasFile('pluginzip')) {
            Storage::disk('s3')->putFileAs($folder, $request->file('pluginzip'), $filename);
        }
        
        $push = new Push;
        $push->type = 'pluginupdate';
        $push->push_title = $version;
        $push->msg = '';
        $push->meta2 = '';
        $push->meta1 = $product;

        $data = array(
            'changelogs' => explode("\n",$request->input('changelogs')),
            'wp_tested' => $request->input('wp_tested'),
            'wp_required' => $request->input('wp_required'),
            's3_dir' =>  "$folder/$filename"
        );

        $push->meta3 = serialize($data);
        $push->subscription = 0;
        $push->release_date = Carbon::now();
        $push->push_date = Carbon::now();
        $push->has_errors = 0;
        $push->status = 'Pending Release';

        $push->save();
        $release = $this->push_to_release($push);
        $release['ind'] =  $request->input('ind');
        return json_encode($release);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function request_download($product, $version) {
        $release = $this->get_release($product, $version); 

        if($release) {
            $s3_url = $release['s3_dir'];

            if(Storage::disk('s3')->exists($s3_url)) {
               return Storage::disk('s3')->download($s3_url, "$product-$version.zip");
            } else return 'File does not exist.';
        } else return 'Release does\'t not exist.';
        
    }

    public function change_status($new_status, $release_id) {
        $release = Push::find($release_id);

        $text_status = str_replace('_', ' ', $new_status);
        $text_status = ucfirst($text_status); 

        $release->status = $text_status;

        $product = Product::where('sku', $release->meta1)->first();

        if($product) {
            $subscription = $product->subscriptions->where('type',$new_status)->first();

            if($subscription) {
                $release->subscription = $subscription->id;
            }

            // Schedule Task
            $config = array(
                'id' => $release_id,
                'status' => 'sns',
                'final_status' => $text_status
            );

            $process = new \App\Tasks\ProcessPushNotices($config);
            $process->schedule('now');
           
        }

        $release->save();
        
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $push = Push::where('id',$id)->first();
        

        if($push) {
            $data = unserialize( $push->meta3 );

            if(Storage::disk('s3')->exists($data['s3_dir'])) {
               Storage::disk('s3')->delete($data['s3_dir']);
            }

            $push->delete();

            return json_encode(array('success' => 1));
        }
        
    }

    public function get_release($product_sku, $version) {
        $push = Push::where('type','pluginupdate')->where('meta1',$product_sku)->where('push_title',$version)->first();

        if($push) {
            return $this->push_to_release($push);
        }
    }

    public function subscriptions_get() {
        return json_encode(\App\Subscription::get());
    }

     public function subscriber_search() {
        return json_encode(\App\Pushsub::where('url','LIKE',"%{$_GET['term']}%")->get());
    }

    public function pushmsgs_get() {
        $pushes = \App\Push::orderBy('id','desc')->get();
        foreach($pushes as $k => $p) {
            $pushes[$k]->push_date = Carbon::parse($p->push_date)->diffForHumans();
            if($p->subscription()->first()) {
                 $pushes[$k]->sent_to = $p->subscription()->first()->title;
             } else {
                 $pushes[$k]->sent_to = '';
             }
           
            if($p['status'] == 'Pending Release') {
                $pushes[$k]->pushed = 0;
                $pushes[$k]->error = 0;
            } else {
                if($p->meta2) {
                    $total = 1;
                } else {
                    $total = $p->subscription()->first()->pushsubs()->count();
                }
                
                $error = $p->pusherrors()->count();

                $pushes[$k]->pushed = $total-$error;
                $pushes[$k]->error = $error;
            }
        }

        return json_encode($pushes);
    }

    public function task_queue() {
        $tasks = \App\QueuedTask::whereIn('status',['open','failed'])
            ->orWhere(function($query){
                $query->whereDate('last_performed', Carbon::now()->toDateString());
            })->get();

        foreach($tasks as $k => $v) {
            $tasks[$k]->data = unserialize($v->data);
        }

        return json_encode(
            $tasks
        );
    }

    public function post_push_notice(Request $request) {
        // Create new Push
        $push = new Push;
        $push->push_title = $request->input('push_title');
        $push->type = $request->input('type');
        $push->msg = !$request->input('msg') ? '' : $request->input('msg');

        $subscriber = $request->input('subscriber');
        if($subscriber && $subscriber != 'spec') {
            $push->meta2 = 0;
            $push->subscription = $request->input('subscriber');
        } else if($subscriber == 'spec') {
            $push->meta2 = $request->input('subscriber_id');
            $push->subscription = 0;
        }

        $push->meta1 = $request->input('meta1','');
        $push->meta3 = $request->input('meta3','');

        $sked = $request->input('when');
        $sked_date = $request->input('whendate');

        if($sked == 'now') {
            $release = Carbon::now();
            $push->status = 'Pending Release';
        } else {
            $release = Carbon::parse($sked_date)->startOfDay();
            $push->status = 'Scheduled';
        }

        $push->release_date = $release;
        $push->push_date = Carbon::now();
        $push->has_errors = 0; 
       
        $push->save();

        // Schedule Task
        $config = array(
            'id' => $push->id,
            'status' => 'sns',
            'final_status' => 'Done'
        );
        $process = new \App\Tasks\ProcessPushNotices($config);

        if($sked == 'now') {
            $process->schedule('now');
        } else {
            $process->schedule($sked_date);
        }

        return '';

    }

    private function push_to_release($p) {
        $r = array();
        $r['version'] = $p->push_title;
        $r['product'] = $p->meta1;
        
        $r['status'] = $p->status;
        $r['date'] = date('Y-m-d H:i:s', strtotime($p->push_date));
        $r['id'] = $p->id;
        $data = unserialize( $p->meta3 );

        $r['wp_required'] = $data['wp_required'];
        $r['changelogs']  = implode("\n", $data['changelogs']);
        $r['wp_tested'] = $data['wp_tested'];
        $r['s3_dir'] = $data['s3_dir'];
        $r['download'] =  Activity::where('type','plugindownload')->where('info1', $p->meta1)->where('info2', $p->push_title)->count();
        $r['autorenew'] =  Activity::where('type','pluginrenew')->where('info1', $p->meta1)->where('info2', $p->push_title)->count();

        return $r;

    }
}
