<?php

namespace App\Http\Controllers;

use App\Pushsub;
use App\License;
use App\Domain;
use App\Push;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use \Carbon\Carbon;

use \GuzzleHttp\Exception\GuzzleException;
use \GuzzleHttp\Client;

class LicenseController extends Controller
{ 

	public function validate_identity(Request $request) {
		$push_enabled = $request->input('get_plugin_update');
		$push_url = $request->input('push_url');
		$post_data = $request->all();

		if($push_enabled) {
			$ps = Pushsub::where('url', $push_url)->first();

			if($ps) {
				return true;
			}
		} else {
			if(isset($post_data['get_plugin_update']) && isset($post_data['push_url'])) {
				return true;
			} 
		}

		return false;
	}	

	public function validate_request(Request $request) {
		$request_ip = $request->input('ip');
		$real_ip = $request->ip();

		return $request_ip == $real_ip;
	}

	public function authenticate_install($install_uri, $license, $activation_key, $require_ak=true) {
		if($require_ak) {
			$find = Domain::where('fullpath','like',$install_uri)->where('activationkey',$activation_key)
					->where('status','active')->first();
			if($find) {
				return true;
			}
		} else {
			$domain = $this->get_rootofdomain($install_uri);
			$find = Domain::where('domain',$domain)->where('status','active')->first();

			if($find) {
				return true;
			}		
		}

		return false;
	}	

	public function get_license_access() {
		
	}	

	public function generate_file_link() {

	}

	public function get_rootofdomain($install_uri) {
		$parsed = parse_url($install_uri);
		$domain = $parsed['host'];

		if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) {
			$domain = $regs['domain'];
		}

		return $domain;
	}

	public function activate_installation($install_uri, $key,$ip) {
		$license = $this->get_license($key);
		if(!$license) return false;

		$activation_key = sha1(time());

		$domain = Domain::firstOrNew(['fullpath' => $install_uri]);
		$domain->domain = $this->get_rootofdomain($install_uri);
		$domain->license_id = $license->id;
		$domain->activationkey = $activation_key;
		$domain->status = 'active';
		$domain->ip = $ip;
		$domain->activation_date = Carbon::now();

		$domain->save();

		return $activation_key;
	}

	public function count_remaining_domains($key) {
		$license = $this->get_license($key);

		if($license) {
			$allowed_domains = $license->domain_limit;
			$domains_used = count($license->domains_grouped());

			return $allowed_domains > $domains_used ? ($allowed_domains - $domains_used) : 0;
		}

		return false;
	}

	public function get_license($key, $status = 'active') {
		return License::where('licensekey', $key)->where('status','active')->first();
	}

	public function request_download_update(Request $request) {
		if(!$this->validate_identity($request)) {
			return json_encode(array('error' => 'Cannot validate the identity of the client. [L0001]'));
		}

		if(!$this->validate_request($request)) {
			return json_encode(array('error' => 'Cannot validate the integrity of the request. [L0002]'));
		}

		$sku = $request->input('product');
		$key = $request->input('key');

		$activation_key = $request->input('activation_key');
		$install_uri = $request->input('site_url');
		$request_ver = $request->input('request_ver');

		// get activation:
		$domain = Domain::where('activationkey', $activation_key)->first();

		if(!$domain || ($domain && $domain->license->licensekey != $key)) {
			return json_encode(array('error' => 'This domain is not activated for this license key. [D0004]'));
		} else {
			$license = $domain->license;

			// Check Version Release if accessible by the license
			$update = Push::where('type','pluginupdate')->where('push_title', $request_ver)->first();

			if(!$update) {
				return json_encode(array('error' => 'Cannot find plugin update. Please click force re-check update. [D0001]'));
			}

			if($update->release_date >= $license->expires) {
				return json_encode(array('error' => 'Your license has already expired and is not allowed to access this version. Please consider renewing your plugin license. [D0002]'));
			} else {
				$data = unserialize( $update->meta3 );
				$s3_url = str_replace('[ver]', $request_ver, $data['s3_dir']);

				$file_expiry = Carbon::now()->addDays(7);
				
				if(strtotime($file_expiry) >= strtotime($license->expires))
					$file_expiry = $license->expires;

				// Get S3 File:
				if(Storage::disk('s3')->exists($s3_url)) {
					$url = Storage::disk('s3')->temporaryUrl(
					    $s3_url, $file_expiry
					);
					return json_encode(array('download_url' => $url, 'expires' => (string) $file_expiry));
				} else {
					return json_encode(array('error' => 'Cannot find plugin update file. Please click force re-check update. [D0003]'));
				}
			}
		}
	}

	public function request_activate_installation(Request $request) {
		$proceed = false;

		if(!$this->validate_identity($request)) {
			return json_encode(array('error' => 'Cannot validate the identity of the client. [L0001]'));
		}

		if(!$this->validate_request($request)) {
			return json_encode(array('error' => 'Cannot validate the integrity of the request. [L0002]'));
		}

		$sku = $request->input('product');
		$key = $request->input('key');
		$install_uri = $request->input('site_url');
		$license = $this->get_license($key);

		if(!$license) {
			return json_encode(array('error' => 'License key is not valid. [L0003]'));
		}

		if($license->product->sku != $sku) {
			return json_encode(array('error' => 'License key is not valid for this product. [L0004]'));
		}

		if($this->authenticate_install($install_uri,$key,false,false)) {
			$proceed = true;
		} else {
			$num_domains = $this->count_remaining_domains($key);

			if($num_domains > 0) {
				$proceed = true;
			} else {
				return json_encode(array('error' => 'Number of domains has been exceeded. Cannot activate. [L0005]'));
			}
		}

		if($proceed) {
			$activation_key = $this->activate_installation($install_uri, $key, $request->ip());

			if(!$activation_key) {
				return json_encode(array('error' => 'Cannot activate installation at this time. Contact Support at https://infusedaddons.com/support [L0006]'));
			} else {
				return json_encode(array('activation_key' => $activation_key));
			}
		}
	}

	public function request_deactivate_installation(Request $request) {
		if(!$this->validate_identity($request)) {
			return json_encode(array('error' => 'Cannot validate the identity of the client. [L0001]'));
		}

		if(!$this->validate_request($request)) {
			return json_encode(array('error' => 'Cannot validate the integrity of the request. [L0002]'));
		}

		Pushsub::where('url','like',$request->input('push_url'))->delete();

		$key = $request->input('key');
		$install_uri = $request->input('site_url');
		$license = $this->get_license($key);
		$ak = $license = $request->input('activation_key');

		if(!$license) {
			return json_encode(array('error' => 'License is not valid. [L0003]'));
		}

		if(!$this->authenticate_install($install_uri,$key,$ak)) {
			return json_encode(array('error' => 'Cannot authenticate domain [L0007]'));
		}

		Domain::where('fullpath','like', $install_uri)->where('status','active')->update(['status' => 'inactive']);
		
		return json_encode(array('success' => 1));
	}

}
