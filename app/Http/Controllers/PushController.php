<?php

namespace App\Http\Controllers;

use App\Pushsub;
use App\Push;
use App\Subscription;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \Carbon\Carbon;

use \GuzzleHttp\Exception\GuzzleException;
use \GuzzleHttp\Client;

class PushController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function subscribe(Request $request) {
        $url = $request->input('receiver_uri');
        $subscription = $request->input('subscription');

        if(!empty($url) && !empty($subscription)) {
            // check if already exist
            $sub = Subscription::where('slug', $subscription)->first();
            if(!$subscription) return false;
            $sub_id = $sub ? $sub->id : 0;

            $ps = Pushsub::where('url', $url)->where('subscription',$sub_id)->first();
            
            if($ps) {
                if($ps->status == 'active') {
                    return 'OK';
                }
            } else {
                $ps = new Pushsub;
                $ps->url = $url;
                $ps->subscription = $sub_id ? $sub_id : 0;
                $ps->status = 'pending';
                $ps->sns_status = 'pending';
                $ps->sns_id = '';
                $ps->sns_unsubscribe = '';
                $ps->license_id = 0;
                $ps->sku = '';
                $ps->last_success = date('Y-m-d H:i:s',0);
            }
            
            $ps->last_push = Carbon::now();
            $ps->save();

            // TEST connection
            $push = new Push;
            $push->push_title    = 'TestPush';
            $push->type     = 'test';
            $push->msg      = 'Test Push from InfusedWoo Update Server';
            $push->release_date = date('Y-m-d H:i:s');
            $push->meta1 = "";
            $push->meta2 = $ps->id;
            $push->meta3 = "";
            $push->subscription = 0; 
            $push->has_errors = 0; 
            $push->status = "";
            $push->push_date = date('Y-m-d H:i:s'); 

            $result = $this->push($push, $ps);

            if($sub) {  
                $topicArn = $sub->snsarn;

                if($topicArn) {
                    $this->sns_register($url, $topicArn);
                }
            }
            

            if($result) {
                $ps->status = 'active';
                $ps->save();
                return 'OK';
            } else {
                return 'Error communicating with client.';
            }
        }
    }

    public function sns_register($url, $topicArn) {
        $parse = parse_url($url);
        \AWS::createClient('sns')->subscribe(array(
            'TopicArn' => $topicArn,
            'Protocol' => ($parse['scheme'] == 'https') ? 'https' : 'http',
            'Endpoint' => $url,
        ));
    }

    public function sns_confirm(Request $request) {
        $url = $request->input('receiver_uri');
        $sns_id = $request->input('sns_id');
        $subscription = $request->input('subscription');
        $unsubscribe_uri = $request->input('sns_unsubscribe');

        if(empty($url) || empty($sns_id)) return false;

        $pushsub = Subscription::where('slug',$subscription)->first()->pushsubs()->where('url', $url)->first();

        if($pushsub) {
            $pushsub->sns_status = 'active';
            $pushsub->sns_id     = $sns_id;
            $pushsub->sns_unsubscribe = !empty($unsubscribe_uri) ? $unsubscribe_uri : '';
            $pushsub->save();
            return 'OK';
        }

        return '';
    }

    public function sns_push(Push $push) {
        $snsarn = $push->subscription()->first()->snsarn;
        $msg = $push->prep_push_msg();

        \AWS::createClient('sns')->publish([
            'Message' => json_encode($msg), // REQUIRED
            'MessageStructure' => 'string',
            'Subject' => $push->push_title,
            'TopicArn' => $snsarn,
        ]);
    }

    public static function push(Push $push, Pushsub $pushsub) {
        $client = new Client();

        try {
            $res = $client->post($pushsub->url, array(
                'form_params' => $push->prep_push_msg()
            ));

            $pushsub->last_push = Carbon::now();

            $statuscode = $res->getStatusCode();
            $response = $res->getBody()->getContents();
        } catch( GuzzleException $e ) {
            $res = $e->getResponse();
            $statuscode = $res->getStatusCode();
            $response = $res->getBody()->getContents();        
        }    

        if($statuscode == 200 && $response == 'OK') {
            $pushsub->last_success = Carbon::now();
            return true;
        } else {
            // REPORT ERROR
            $push->report_error($pushsub, 'Response Error: Status Code = ' . $statuscode . ', ResponseBody = ' . $response);
            return false;
        }

        if($pushsub->id) {
             $pushsub->save();
        }
       
    }

    public function schedule_push(Push $push) {
        $task = new App\Tasks\ProcessPushNotices;
        $task->data(
            array(
                'status' => 'sns',
                'id' => $push->id
            )
        );

        $task->schedule('now');
    }   

    public function feed($sku) {
        if(strpos($sku, 'test') !== false) return '[]';

        $feed = Push::where('meta1',$sku)->orderBy('push_date','desc')->get();

        foreach($feed as $k => $f) {
            $feed[$k]->meta3 = unserialize($f->meta3);
        }

        return json_encode($feed);
    }
}