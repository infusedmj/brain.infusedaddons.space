<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pushsub extends Model
{
    protected $fillable = [
    	'license_id',
		'url',
		'status',
		'subscription',
		'sku',
		'last_push',
		'last_success'
    ];

    public function pusherrors()
    {
        return $this->hasMany('App\Pusherror');
    }

    public function license() {
        return $this->belongsTo('App\License');
    }

}
