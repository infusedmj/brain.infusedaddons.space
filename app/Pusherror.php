<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pusherror extends Model
{
    protected $fillable = [
    	'pushsub_id',
		'push_id',
		'errorinfo',
		'errortimes',
		'status'
    ];

    public function pushsub() {
        return $this->belongsTo('App\Pushsub');
    }

    public function push() {
        return $this->belongsTo('App\Push');
    }
}
