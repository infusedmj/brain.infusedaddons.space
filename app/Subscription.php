<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $fillable = [
    	'product_id',
		'type',
		'slug',
		'title',
		'snsarn',
		'last_push',
    ];

    public function pushsubs()
    {
        return $this->hasMany('App\Pushsub','subscription');
    }

    public function pushnotices() {
        return $this->hasMany('App\Push','subscription');
    }


    public function create_new($product_id, $slug, $type, $title) {
        // Create new Topic in Amazon
        $awscall = \AWS::createClient('sns')->createTopic([
                'Name' => $slug, 
            ]);

        $arn = $awscall['TopicArn'];

        $this->product_id = $product_id;
        $this->slug = $slug;
        $this->type = $type;
        $this->title = $title;
        $this->snsarn = $arn;

        $this->save();
    }
}
