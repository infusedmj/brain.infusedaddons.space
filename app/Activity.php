<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
	protected $table = 'activities';
    protected $fillable = [
        'account_id',
		'type',
		'contact_id',
		'domain_id',
		'license_id',
		'info1',
		'info2',
		'info3',
		'info4',
		'info5'
    ];

    public function account() {
        return $this->belongsTo('App\Account');
    }

    public function contact() {
        return $this->belongsTo('App\Contact');
    }

    public function domain() {
        return $this->belongsTo('App\Domain');
    }

    public function license() {
        return $this->belongsTo('App\License');
    }

}
