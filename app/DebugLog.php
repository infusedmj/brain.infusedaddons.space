<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DebugLog extends Model {
	protected $fillable = array('src', 'type', 'msg', 'severity','microtime','backtrace','client','temp');
}