<?php

namespace App;

use Illuminate\Support\Facades\DB;
use \Carbon\Carbon;

class WorkerManager {
	private static $max_concurrent_workers = 4;
	private static $work_on_cron_or_admin_only = false;
	private static $worker_transient_prefix = '_worker_';

	private static $worker_lifetime_sec = 20;
	private static $deployed_worker = false;

	public static function maybe_deploy_worker() {
		if(!self::$deployed_worker) {
			$allowed_workers_no = self::get_allowed_number_of_workers();
			$expiration = self::get_worker_lifetime_sec();

			for($i=1; $i<=$allowed_workers_no; $i++) {
				$worker_id = self::$worker_transient_prefix . $i;
				$is_running = Option::get( $worker_id );

				if(!$is_running) {
					self::$deployed_worker = true;
					Option::set( $worker_id,  true, ($expiration+5));
					$worker = new Worker($expiration);
					Option::set( $worker_id,  false);
					break;
				}
			}
		}		
	}

	private static function get_allowed_number_of_workers() {
		return Option::get('worker_max_concurrent',self::$max_concurrent_workers);
	}

	private static function get_worker_lifetime_sec() {
		return Option::get('worker_lifetime_sec',self::$worker_lifetime_sec);
	}

	public static function get_next_available_task() {
		#logic - priority scheduled, recurrring and queue
		# then scheduled, recurring and queue

		// logic get priority first and then non-priority
		$priority_task = self::get_next_task(true);

		if($priority_task === false) {
			$task = self::get_next_task();
		} else {
			$task = $priority_task;
		}

		return $task;
	}

	private static function get_next_task($priority = false) {
		$tasks = array();


		$p = $priority ? 1 : 0;

		$scheduled_tasks = QueuedTask::where('scheduled_exec', '<=', Carbon::now())
										->whereIn('status', array('scheduled','failed'))
										->where('priority', $p)
										->where('lck', '!=', 1)
										->orderBy('scheduled_exec', 'asc')
										->orderBy('id', 'asc')
										->first();

		if($scheduled_tasks) $tq = $scheduled_tasks;
		else {
			// unscheduled recurring tasks:
			$queued_tasks = QueuedTask::whereIn('status', array('open'))
											->where('priority', $p)
											->where('lck', '!=', 1)
											->orderBy('id', 'asc')
											->first();

			if($queued_tasks)  $tq = $queued_tasks;
		}
		// remember to lock first

		if(isset($tq)) {
			$tq->lock();

			$class = '\\' . $tq->task;

			if(class_exists($class)) {
				$data = Util::maybe_unserialize( $tq->data );
				$task = new $class($data, $tq);

				if($tq->status == 'scheduled') {
					$tq->status = 'open';
					$tq->save();
				}
				return $task;
			} else {
				$tq->report_error('Cannot Find Task ' . $tq->task, 'error');
				$tq->unlock();
				return $this->get_next_task();
			}
		} else {
			DB::commit();
		}

		
		return false;
	}

	public static function get_memory_limit() {
		if ( function_exists( 'ini_get' ) ) {
			$memory_limit = ini_get( 'memory_limit' );
		} else {
			// Sensible default.
			$memory_limit = '128M';
		}
		if ( ! $memory_limit || -1 === $memory_limit ) {
			// Unlimited, set to 32GB.
			$memory_limit = '32000M';
		}
		return intval( $memory_limit ) * 1024 * 1024;
	}
}