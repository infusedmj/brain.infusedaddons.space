<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
    	'name',
		'sku',
		'status',
		'downloadurl1',
		'downloadurl2',
		'downloadurl3',
		'guideurl1',
		'guideurl2'
    ];

    public function licenses()
    {
        return $this->hasMany('App\License');
    }

    public function subscriptions()
    {
        return $this->hasMany('App\Subscription');
    }

}
